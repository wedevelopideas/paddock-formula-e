<?php

use Faker\Generator as Faker;

$factory->define(\App\Paddock\EPrixs\Models\EPrixs::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug,
        'hashtag' => $faker->name,
    ];
});
