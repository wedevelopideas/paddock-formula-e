<?php

namespace App\Paddock\EPrixs\Repositories;

use App\Paddock\EPrixs\Models\EPrixs;

class EPrixsRepository
{
    /**
     * @var EPrixs
     */
    private $EPrixs;

    /**
     * EPrixsRepository constructor.
     * @param EPrixs $EPrixs
     */
    public function __construct(EPrixs $EPrixs)
    {
        $this->EPrixs = $EPrixs;
    }
}
