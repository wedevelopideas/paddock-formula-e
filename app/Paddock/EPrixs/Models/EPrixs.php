<?php

namespace App\Paddock\EPrixs\Models;

use Illuminate\Database\Eloquent\Model;

class EPrixs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'e_prixs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'hashtag',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
